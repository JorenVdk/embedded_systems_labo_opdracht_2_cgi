$(document).ready(function(){
   
    $("#fetchBtn").click(function(){
        var nameOfUser = getCookie("username");
        var link = 'http://raspv/Labo_Opdracht_2_uitbreiding/'+nameOfUser+'_colors.json'
        $.ajax({
            url:link,
            type:'HEAD',
            error: function()
            {
                fetch('colors.json')
                    .then(function(response) {
                        return response.json();
                    })
                    .then(function(myJson) {
                        //var jsonFIle = myJson;
                        $("#jsonTable tr").remove();
                        AddJsonToTable(myJson);
                });
            },
            success: function()
            {
                fetch(nameOfUser + '_colors.json')
                    .then(function(response) {
                        return response.json();
                    })
                    .then(function(myJson) {
                        //var jsonFIle = myJson;
                        $("#jsonTable tr").remove();
                        AddJsonToTable(myJson);
                });
            }
        });        
    });

    checkCookie();

});

function AddJsonToTable(jsonFile){
    var jsonData = jsonFile;
    console.log(jsonData);

    var columns = addAllColumnHeaders(jsonData, '#jsonTable > thead');
  
    for (var i = 0; i < jsonData.length; i++) {
      var row$ = $('<tr/>');
      for (var colIndex = 0; colIndex < columns.length; colIndex++) {
        var cellValue = jsonData[i][columns[colIndex]];
        if (cellValue == null) cellValue = "";
        row$.append($('<td/>').html(cellValue));
      }
      $('#jsonTable > tbody').append(row$);
    }
}
  
// Adds a header row to the table and returns the set of columns.
// Need to do union of keys from all records as some records may not contain
// all records.
function addAllColumnHeaders(jsonData, selector) {
    var columnSet = [];
    var headerTr$ = $('<tr/>');

    for (var i = 0; i < jsonData.length; i++) {
        var rowHash = jsonData[i];
        for (var key in rowHash) {
            if ($.inArray(key, columnSet) == -1) {
                columnSet.push(key);
                headerTr$.append($('<th/>').html(key));
            }
        }
    }
    $(selector).append(headerTr$);

    return columnSet;
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function checkCookie() {
    var user = getCookie("username");
    if (user != "") {
        //alert("Welcome again " + user);
    } else {
        user = prompt("Please enter your name:", "");
        if (user != "" && user != null) {
            setCookie("username", user, 365);
        }
    }
}