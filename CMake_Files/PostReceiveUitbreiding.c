#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <time.h>
#include <unistd.h>
#include "jsmn.h"

#define MAXLEN 200

// Path to json file in the map with site in it
#define JSON_FILE_PATH "/var/www/html/Labo_Opdracht_2_uitbreiding/colors.json"
#define BUFFER_SIZE 5000
#define MAX_TOKEN_COUNT 128

// Decode the data that comes from the post
void unencode(char *src, char *last, char *dest)
{
    for(; src != last; src++, dest++)
        if(*src == '+'){
            *dest = ' ';
        }     
        else if(*src == '%') {
            int code;
            if(sscanf(src+1, "%2x", &code) != 1){
            code = '?';
            } 
            *dest = code;     
            src +=2;
        }     
        else{
            *dest = *src;
        }    
        *dest = '\n';
        *++dest = '\0';
}

// Read the json file
void readfile(char* filepath, char* fileContent)
{
    FILE *f;
    int c;
    int index = 0;
    f = fopen(filepath, "rt");
    while((c = fgetc(f)) != EOF){
        fileContent[index] = c;
        index++;
    }
    fileContent[index] = '\0';
    fclose(f);
}

// Get the data from the json file and paste it together with the new data
int parseJSON(char *filepath, char** dataJson){

    time_t current_time;
    char* c_time_string;

    /* Obtain current time. */
    current_time = time(NULL);

    if (current_time == ((time_t)-1))
    {
        (void) fprintf(stderr, "Failure to obtain the current time.\n");
        exit(EXIT_FAILURE);
    }

    /* Convert to local time format. */
    c_time_string = ctime(&current_time);
    if (c_time_string == NULL)
    {
        (void) fprintf(stderr, "Failure to convert the current time.\n");
        exit(EXIT_FAILURE);
    }

    strtok(c_time_string, "\r\n");
    strtok(dataJson[0], "\r\n");
    strtok(dataJson[1], "\r\n");
    strtok(dataJson[2], "\r\n");
    strtok(dataJson[3], "\r\n");
    strtok(dataJson[4], "\r\n");

    //Name of the user
    strtok(dataJson[5], "\r\n");
    
    char fileOfUser[5000];
    strcpy(fileOfUser, "/var/www/html/Labo_Opdracht_2_uitbreiding/");
    strcat(fileOfUser, dataJson[5]);
    strcat(fileOfUser, "_colors.json");

    char JSON_STRING[BUFFER_SIZE];

    char value[1024];
    char key[1024];

    // Check if file exist, if not make a new file
    if( access( fileOfUser, F_OK ) != -1 ) {
        printf("file exist\r\n");
        readfile(fileOfUser, JSON_STRING);
    } else {
        printf("file does not exist\r\n");
        readfile(JSON_FILE_PATH, JSON_STRING);
        FILE *TheFile;
        TheFile = fopen(fileOfUser, "a");
        fclose(TheFile); 
    } 

    int i;
    int r;

    jsmn_parser p;
    jsmntok_t t[MAX_TOKEN_COUNT];

    jsmn_init(&p);

    r = jsmn_parse(&p, JSON_STRING, strlen(JSON_STRING), t, sizeof(t)/(sizeof(t[0])));

    if (r < 0) {
        printf("Failed to parse JSON: %d\n", r);
        return 1;
    }

    /* Assume the top-level element is an array */
    if (r < 1 || t[0].type != JSMN_ARRAY) {
        printf("Array expected\n");
        return 1;
    }

    //Shows the whole array in one time and writes to another json
    jsmntok_t json_value = t[0];
    int string_length = json_value.end - json_value.start;

    int idx;

    for (idx = 0; idx < string_length; idx++){
        value[idx] = JSON_STRING[json_value.start + idx ];
    }
    value[string_length] = '\0';
    // 'value' is now the whole json array that we get from colors.json at first, 
    // after that it comes from the user json file
    
    char buffer[5000];

    // Example of data getting from a post
    char str[5000];
    strcpy(str, ",\r\n");
    strcat(str, "    {\r\n");
    sprintf(buffer, "        \"color\": \"%s\",\r\n", dataJson[0]);
    strcat(str,buffer);
    sprintf(buffer, "        \"category\": \"%s\",\r\n", dataJson[1]);
    strcat(str,buffer);
    sprintf(buffer, "        \"type\": \"%s\",\r\n", dataJson[2]);
    strcat(str,buffer);
    sprintf(buffer, "        \"rgba\": \"%s\",\r\n", dataJson[3]);
    strcat(str,buffer);
    sprintf(buffer, "        \"hex\": \"%s\",\r\n", dataJson[4]);
    strcat(str,buffer);
    sprintf(buffer, "        \"time\": \"%s\"\r\n", c_time_string);
    strcat(str,buffer);
    strcat(str, "    }\r\n");
    strcat(str, "]");
    strcat(str, "\0");

    // Delete the ] character to append the extra data and after that append a new ]
    int idxToDel = string_length-3; 
    memmove(&value[idxToDel], &value[idxToDel + 3], strlen(value) - idxToDel);
    strcat(value,str);

    FILE *fp = fopen(fileOfUser, "w");
    if (fp == NULL)
    {
        printf("Error opening file!\n");
        exit(1);
    }
    
    fprintf(fp, "%s", value);
    fclose(fp);    

    return 0;
}

int main(void)
{
    char *lenstr;
    char input[MAXLEN], data[MAXLEN];
    long len;
    char** tokens; 

    printf("%s%c%c\n","Content-Type:text/html;charset=iso-8859-1",13,10);
    printf("<TITLE>Response</TITLE>\n");
    lenstr = getenv("CONTENT_LENGTH");
    if(lenstr == NULL || sscanf(lenstr,"%ld",&len)!=1 || len > MAXLEN)
        printf("<P>Error in invocation - wrong FORM probably.");
    else {
        fgets(input, len+1, stdin);
        unencode(input, input+len, data);

        char* pch;
        char* stringArray[6];
        int i = 0;
        int j = 0;

        pch = strtok (data,"&=");
        while (pch != NULL)
        {
            if((i%2)==1)
            {
                stringArray[j] = pch;
                j++;
            }
            pch = strtok (NULL, "&="); 
            i++;  
        }

        parseJSON(JSON_FILE_PATH, stringArray);

        // Redirect to your previous page
        const char * redirect_page_format =
        "<html>\n"
        "<head>\n"
        "<meta http-equiv=\"REFRESH\"\n"
        "content=\"0;url=%s\">\n"
        "</head>\n"
        "</html>\n";
        printf (redirect_page_format, getenv("HTTP_REFERER"));
    }
    return 0;
}